'use strict';

// include all required package
const	{ series } 	= require('gulp');
var 	gulp		= require('gulp'),				// gulp base file
		concat		= require('gulp-concat'),		// join multiple files
		mustache 	= require("gulp-mustache"),		// mustache template
		htmlmin 	= require('gulp-html-minifier'),// html minifier
		copy 		= require('recursive-copy'),	// recursive file copy
		fs			= require('fs'),				// file read/write
		uglify 		= require('gulp-uglify-es').default,		// js minify
		less		= require('gulp-less');			// css minify

// set script mode
var env 	= JSON.parse(fs.readFileSync('../develop/config/environment.conf.json'));
var mode 	= require('gulp-mode')({				// script running mode
	modes: ["production", "development"],
	default: "development",
	verbose: false									// show verbose messages
});

// set all configurations
var config = {};									// load all configurations

	config.site		= JSON.parse(fs.readFileSync('../develop/config/site.conf.json'));
	config.page		= JSON.parse(fs.readFileSync('../develop/config/pages.conf.json'));
	config["mode"]	= mode.development()?"development":"production";	
	config['site']  = Object.assign(config['site'], env[config["mode"]]);
	
	console.log("============ this script is running in " + config["mode"] + " mode ============");

// gulp custom functions
function build_pages(cb) {

	// build pages
	var noOfRecords = config.page["page"].length;		
		
	config.page["page"].forEach(function(page, i) {

		page.config = config;
				
		gulp.src([
			"../develop/template/header.mustache",
			"../develop/template/" + page['name'] + ".mustache",
			"../develop/template/footer.mustache"
		])
		.pipe(concat(page["filename"]))
		.pipe(mode.production(htmlmin({collapseWhitespace: true})))
		.pipe(mustache(page, { 
			tags:['{[',']}']
		}))
		.pipe(gulp.dest( config.site["path"] +  page['path']))
		.on('end', function(){	

			noOfRecords--;
			if(noOfRecords==0){
				cb();
			}
			
		});
	
	});		
	
}

function optimize_js_library(cb) {
		
	gulp.src(["../develop/include/js/**/*.js"])
		.pipe(mode.production(uglify()))
		.pipe(gulp.dest(config.site["path"] + "include/js/"))
		.on('end', function(){ 
			cb();		
		});
	
}

function optimize_css_library(cb) {
		
	gulp.src(["../develop/include/css/**/*.css"])
		.pipe(mode.production(less()))
		.pipe(gulp.dest(config.site["path"] + "include/css/"))
		.on('end', function(){ 
			cb();		
		});
	
}

function publish_images(cb) {
 
	copy("../develop/include/img", config.site["path"] + "include/img", {		
		overwrite: true
	})
    .then(function(results) {
        cb();
    })
    .catch(function(error) {
        console.error('Copy failed: ' + error);
		cb();
    });	    
	
}

function publish_vendor_library(cb) {
 
	copy("../develop/include/vendor", config.site["path"] + "include/vendor", {		
		overwrite: true
	})
    .then(function(results) {
        cb();
    })
    .catch(function(error) {
        console.error('Copy failed: ' + error);
		cb();
    });	    
	
}

function publish_seo(cb) {
 
	copy("../develop/seo/", config.site["path"], {		
		overwrite: true
	})
    .then(function(results) {
        cb();
    })
    .catch(function(error) {
        console.error('Copy failed: ' + error);
		cb();
    });	    
	
}

exports.default = series(
	build_pages,
	optimize_js_library,
	optimize_css_library,
	publish_images,
	publish_vendor_library,
	publish_seo
);