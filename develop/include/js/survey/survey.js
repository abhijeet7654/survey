
function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1);
  } else {
      document.addEventListener("DOMContentLoaded", fn);
  }
} 

docReady(function() {

  vm = new Vue({
    el:"#app",
    //name: 'Survey',
    async created() {
      // this.getAllSymtoms();
    },
    data() {
      return {
        title: "survey",
        slideprop: {
            currentslide : 1,
            totalslide : 5			
        },
        slide: [
          { 
            id: 4,
            heading: "Direct Contact",
            question: "Any Direct Contact with Confirmed or Presumtive case of CORONAVIRUS in last 14 days?",
            item: [
              { 
                type: "radio",
                isenabled: true,
                name: "close_contact",
                id: 4001,
                label: ["yes", "no"],
                checked:false,
                score: 5
              }
            ]
          },
          { 
            id: 1,
            heading:"Symptoms",
            question: "Are you experiencing any <i>new</i> or <i>worsening</i> symptoms?",
            item: [              
              { 
                type: "checkbox",
                isenabled: true,
                id: 1003,
                name: "breathing",
                label: "Difficulty breathing or shortness of breath",
                checked: false,
                score: 2
              },
              { 
                type: "checkbox",
                isenabled: true,
                id: 1004,
                name: "cough",
                label: "Cough",
                checked: false,
                score: 2
              },            
              { 
                type: "checkbox",
                isenabled: true,
                id: 1005,
                name: "swallowing",
                label: "Difficulty swallowing",
                checked: false,
                score: 2
              },            
              { 
                type: "checkbox",
                isenabled: true,
                id: 1006,
                name: "taste",
                label: "Loss of taste/smell",
                checked: false,
                score: 2
              },           
              { 
                type: "checkbox",
                isenabled: true,
                id: 1007,
                name: "pneumonia",
                label: "New diagnosis of pneumonia",
                checked: false,
                score: 2
              },   
              { 
                type: "checkbox",
                isenabled: true,
                id: 1008,
                name: "throat",
                label: "Sore throat or hoarse voice",
                checked: false,
                score: 2
              },            
              { 
                type: "checkbox",
                isenabled: true,
                id: 1009,
                name: "nose",
                label: "Runny nose or sneezing or nasal congestion",
                checked: false,
                score: 2
              },            
              { 
                type: "checkbox",
                isenabled: true,
                id: 1010,
                name: "digestion",
                label: "Digestive symptoms (including nausea, vomiting, diarrhea, abdominal pain)",
                checked: false,
                score: 2
              },
              { 
                type: "checkbox",
                isenabled: true,
                name: "malaise",
                id: 2001,
                label: "Unexplained fatigue/malaise",
                checked: false,
                score: 2
              },
              { 
                type: "checkbox",
                isenabled: true,
                name: "chills",
                id: 2002,
                label: "Chills",
                checked: false,
                score: 2
              },
              { 
                type: "checkbox",
                isenabled: true,
                name: "headache",
                id: 2003,
                label: "Different or worsening headaches",
                checked: false,
                score: 2
              },        
              { 
                type: "checkbox_reset",
                isenabled: true,
                id: 1011,
                name: "none",
                label: "None of Above",
                checked: false,
                score: 0
              }
            ]
          },
          { 
            id: 6,
            heading: "Fever",
            question: "Do you have any fever in past 24 hours or feeling hot or feverish(Temp equal or above 100F) ?",
            item: [
              { 
                type: "radio",
                isenabled: true,
                name: "fever",
                id: 6001,
                label: ["yes", "no"],
                checked:false,
                score: 5
              }
            ]
          },
          { 
            id: 3,
            heading: "Sick at Home",
            question: "Anyone currently sick at your home with any of the above symptoms?",
            item: [
              { 
                type: "radio",
                isenabled: true,
                name: "fevhomeer",
                id: 3001,
                label: ["yes", "no"],
                checked:false,
                score: 5
              }
            ]
          },
          { 
            id: 5,
            heading: "Travel",
            question: "Have you traveled internationally or domestically within the past 14 days?",
            item: [
              { 
                type: "radio",
                isenabled: true,
                name: "travel",
                id: 5001,
                label: ["yes", "no"],
                checked:false,
                score: 5
              }
            ]
          },
    
        ]
      }
    },
    mounted:function(){
          this.swiper = this.initslider();						
    },
    methods:{
      async getAllSymtoms() {      
        const todos = await API.graphql({
          query: listSymtomss,
          variables:{
            filter:{
              Enabled:{
                eq:"Y"
              }
            }
          }        
        });
        this.symtoms = todos.data.listSymtomss.items;
      },
      initslider:function(){
        const self = this;
        var swiper = new window.Swiper ('.swiper-container', {
          direction: 'horizontal',
          loop: false,
          allowTouchMove: false,
          on: {
            init: function(){
              // console.log(this);
            },
            slideChange: function(s){
              document.body.scrollTop = 0;
              document.documentElement.scrollTop = 0;         
              self.slideprop.currentslide = this.realIndex + 1;
            }
          }
        });
        
        return swiper;
      },
      prevpage: function(){
        if(this.slideprop.currentslide>1){
          this.slideprop.currentslide--;			
          this.swiper.slidePrev();
        }
      },
      nextpage: function(){	
        if(this.slideprop.currentslide<this.slideprop.totalslide){
          this.slideprop.currentslide++;				
          this.swiper.slideNext();
        }
      },
      resetLastCheckbox: function(checkboxValue){
        if(checkboxValue == true){
          let index = this.slideprop.currentslide - 1;
          let itemLength = this.slide[index].item.length-1;
          if(
            (this.slide[index].item[itemLength].checked == true) &&
            (this.slide[index].item[itemLength].type == "checkbox_reset")
            ){
            this.slide[index].item[itemLength].checked = false;
          }
        }
      },
      resetOtherCheckbox: function(checkboxValue){
        if(checkboxValue == true){

          let index = this.slideprop.currentslide - 1;
          let itemLength = this.slide[index].item.length-1; 
          for(let i=0; i< itemLength; i++){
            this.slide[index].item[i].checked = false;
          }

        }
      },
      validate: function(){
            
        var totalscore = 0;

        for(let i=0; i<this.slide.length;i++){
          for(let j=0; j<this.slide[i]['item'].length; j++){
            if(this.slide[i]['item'][j].checked == true){
              totalscore += this.slide[i]['item'][j].score;
            }else if(this.slide[i]['item'][j].checked == "Yes"){
              totalscore += this.slide[i]['item'][j].score;
            }
          }
        }

        // console.log("final score -"+ totalscore);

        if(totalscore < 5 ){
          window.location='./survey-pass.html';
        }else {
          window.location='./survey-fail.html';
        }

      }
    },
    computed: {
      progressWidth() {
        return Math.round((this.slideprop.currentslide / this.slideprop.totalslide) * 100);
      },
      isNxtDisabled: function(){
        
        let btnDisable = true;
        let index = this.slideprop.currentslide - 1;

        if(this.slide[index].item[0].type == "radio"){
          if(this.slide[index].item[0].checked != false){
            btnDisable = false;
          }
        } else {

          for(let i=0; i< this.slide[index].item.length; i++){
            if( this.slide[index].item[i].checked == true){
              btnDisable = false;
              break;
            }
          }
        }

        return btnDisable; // true ==> disable
      }
    }
  });

});